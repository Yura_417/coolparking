﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService, IHostedService
    {
        private Timer _Timer;
        private Timer _TimerLog;
        private double interval = Settings.writeOff;
        private double logInterval = Settings.recordingPeriod;
        public static Parking parking =  ParkingService.Parking;
        public static LogService logService = new LogService();
        public static List<TransactionInfo> listTransaction = new List<TransactionInfo>();
        private static TransactionInfo transactions;

        public double Interval { get { return interval; } set { interval = value; } }

        public event ElapsedEventHandler Elapsed;

        public void FireElapsedEvent()
        {
            _Timer.Elapsed += OnTimedEvent;
            _TimerLog.Elapsed += OnLogEvent;
            Elapsed?.Invoke(this, null);
        }

        public TimerService()
        {
            _Timer = new Timer();
            _Timer.Elapsed += OnTimedEvent; 
            _Timer.Enabled = false;
            _Timer.Interval = Interval;

            _TimerLog = new Timer();
            _TimerLog.Elapsed += OnLogEvent;
            _TimerLog.Enabled = false;
            _TimerLog.Interval = logInterval;

            parking.ParkingBalanse = 0;
            listTransaction.Clear();
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            foreach (var obj in parking.Vehicles)
            {
                //PassengerCar
                if(obj.VehicleType == VehicleType.PassengerCar.ToString())
                {
                    if (obj.Balance <= 0)
                    {
                        decimal fine = (Settings.TariffPassengerCar * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else if (obj.Balance > 0 && (obj.Balance - Settings.TariffPassengerCar) < 0)
                    {
                        decimal fine = obj.Balance + ((Settings.TariffPassengerCar - obj.Balance) * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else
                    {
                        obj.Balance -= Settings.TariffPassengerCar;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += Settings.TariffPassengerCar;
                        Settings.initialBalance += Settings.TariffPassengerCar;
                    }
                }
                //Truck
                else if(obj.VehicleType == VehicleType.Truck.ToString())
                {
                    if (obj.Balance <= 0)
                    {
                        decimal fine = (Settings.TariffTruck * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else if (obj.Balance > 0 && (obj.Balance - Settings.TariffTruck) < 0)
                    {
                        decimal fine = obj.Balance + ((Settings.TariffTruck - obj.Balance) * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else
                    {
                        obj.Balance -= Settings.TariffTruck;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += Settings.TariffTruck;
                        Settings.initialBalance += Settings.TariffTruck;
                    }
                }
                //Bus
                else if (obj.VehicleType == VehicleType.Bus.ToString())
                {
                    if (obj.Balance <= 0)
                    {
                        decimal fine = (Settings.TariffBus * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else if (obj.Balance > 0 && (obj.Balance - Settings.TariffBus) < 0)
                    {
                        decimal fine = obj.Balance + ((Settings.TariffBus - obj.Balance) * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else
                    {
                        obj.Balance -= Settings.TariffBus;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += Settings.TariffBus;
                        Settings.initialBalance += Settings.TariffBus;
                    }
                }
                //Motorcycle
                else if (obj.VehicleType == VehicleType.Motorcycle.ToString())
                {
                    if (obj.Balance <= 0)
                    {
                        decimal fine = (Settings.TariffMotorcycle * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else if (obj.Balance > 0 && (obj.Balance -Settings.TariffMotorcycle) < 0)
                    {
                        decimal fine = obj.Balance + ((Settings.TariffMotorcycle - obj.Balance) * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else
                    {
                        obj.Balance -= Settings.TariffMotorcycle;
                        transactions = new TransactionInfo(obj, obj.Balance, DateTime.Now);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += Settings.TariffMotorcycle;
                        Settings.initialBalance += Settings.TariffMotorcycle;
                    }
                }

            }
        }

        private static void OnLogEvent(Object source, ElapsedEventArgs e)
        {
            foreach (var obj in listTransaction.ToArray())
            {
                logService.Write($"Зняття у {obj.Vehicle}         час: {obj.Time}   залишок: {obj.Sum}");
            }
            listTransaction.Clear();
            parking.ParkingBalanse = 0;
        }

        public void Dispose()
        {
            _Timer.Dispose();
            _TimerLog.Dispose();
        }

        public void Start()
        {
            _Timer.Enabled = true;
            _TimerLog.Enabled = true;
        }

        public void Stop()
        {
            _Timer.Enabled = false;
            _TimerLog.Enabled = false;
        }

        public Task StartAsync(System.Threading.CancellationToken cancellationToken)
        {
            Start();
            return Task.CompletedTask;
        }

        public Task StopAsync(System.Threading.CancellationToken cancellationToken)
        {
            Stop();
            return Task.CompletedTask;
        }
    }
}
