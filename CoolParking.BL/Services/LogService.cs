﻿using CoolParking.BL.Interfaces;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath => $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public string logPath = string.Empty;

        public LogService(string logFilePath)
        {
            logPath = logFilePath;
        }

        public LogService() { }

        public string Read()
        {
            StreamReader sr = new StreamReader(LogPath);
            string line = sr.ReadToEnd();
            sr.Close();
            return line;         
        }

        public void Write(string logInfo)
        {
            if (logPath == null || logPath == "")
            {
                logPath = LogPath;
            }

            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            logFileInfo = new FileInfo(logPath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else 
            {
                fileStream =  new  FileStream(LogPath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(logInfo);
            log.Close();
        }
    }
}
