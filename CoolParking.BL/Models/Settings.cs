﻿namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal initialBalance { get; set; } = 0;
        public static int Capacity { get; set; } = 10;
        public static int writeOff = 5000;
        public static int recordingPeriod = 60000;
        public static decimal TariffPassengerCar = 2;
        public static decimal TariffTruck = 5;
        public static decimal TariffBus = 3.5m;
        public static decimal TariffMotorcycle = 1;
        public static decimal coefficient = 2.5m;
    }
}
