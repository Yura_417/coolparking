﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string Vehicle { get; set; }
        public decimal Sum { get; set; }
        public DateTime Time { get; set; }

        //public Vehicle Vehicle
        //{
        //    get { return vehicle; }
        //    set
        //    {
        //        if (value == null)
        //        {
        //            throw new ArgumentNullException("Vehicle");
        //        }
        //        vehicle = value;
        //    }
        //}

        public TransactionInfo(Vehicle vehicle, decimal sum, DateTime time)
        {
            Vehicle = vehicle.Id;
            Sum = sum;
            Time = time;
        }

    }
}
