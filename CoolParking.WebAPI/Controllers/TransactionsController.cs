﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [Route("~/api/[controller]/last")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TransactionInfo>>> GetLastTransactions()
        {
            try
            {
                IEnumerable<TransactionInfo> transactions = _parkingService.GetLastParkingTransactions();
                if (transactions == null)
                {
                    return NotFound();
                }
                return new ObjectResult(transactions);
            }
            catch
            {
                return BadRequest();
            }
        }

        [Route("~/api/[controller]/all")]
        [HttpGet]
        public async Task<ActionResult> GetAllTransaction()
        {
            try
            {
                return new ObjectResult(_parkingService.ReadFromLog());
            }
            catch
            {
                return BadRequest();
            }
        }

        [Route("~/api/[controller]/topUpVehicle")]
        [HttpPut]
        public async Task<ActionResult> PutTopUpVehicle(string id, int sum)
        {
            try
            {
                Vehicle vehicle = await _parkingService.GetVehiclesId(id);
                if (vehicle == null)
                    return NotFound();
                _parkingService.TopUpVehicle(id, sum);

                return Ok(vehicle);
            }
            catch
            {
                return BadRequest();
            }
        }

    }
}
