﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Vehicle>>> GetVehicle()
        {
            try
            {
                IEnumerable<Vehicle> vehicle = _parkingService.GetVehicles();
                if (vehicle == null)
                {
                    return NotFound();
                }
                return new ObjectResult(vehicle);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Vehicle>> GetVehicleById( string id)
        {
            try
            {
                Vehicle vehicle = await _parkingService.GetVehiclesId(id);
                if (vehicle == null)
                    return NotFound();
                return new ObjectResult(vehicle);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<ActionResult<Vehicle>> Post(Vehicle vehicle)
        {
            try
            {
                _parkingService.AddVehicle(vehicle);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Vehicle>> Delete(string id)
        {
            try
            {
                Vehicle vehicle = await _parkingService.GetVehiclesId(id);
                if (vehicle == null)
                    return NotFound();
                _parkingService.RemoveVehicle(vehicle.Id);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
