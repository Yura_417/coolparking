﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [Route("~/api/[controller]/balance")]
        [HttpGet]
        public async Task<ActionResult> GetBalance()
        {
            try
            {
                return new ObjectResult(_parkingService.GetBalance());
                //return Ok(_parkingService.GetBalance());
            }
            catch
            {
                return BadRequest();
            }
        }

        [Route("~/api/[controller]/capacity")]
        [HttpGet]
        public async Task<ActionResult> GetCapacity()
        {
            try
            {
                return new ObjectResult(_parkingService.GetCapacity());
            }
            catch
            {
                return BadRequest();
            }
        }

        [Route("~/api/[controller]/freePlaces")]
        [HttpGet]
        public async Task<ActionResult> GetFreePlaces()
        {
            try
            {
                return new ObjectResult(_parkingService.GetFreePlaces());
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
