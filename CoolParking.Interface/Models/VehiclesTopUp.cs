﻿using Newtonsoft.Json;

namespace CoolParking.Interface.Models
{
    public class VehiclesTopUp
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("sum")]
        public int Sum { get; set; }
    }
}
