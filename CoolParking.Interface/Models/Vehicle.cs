﻿using Newtonsoft.Json;

namespace CoolParking.Interface.Models
{
    public class Vehicle
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public string VehicleType { get; set; }
        [JsonProperty("balanse")]
        public decimal Balance { get; set; }

    }
}
