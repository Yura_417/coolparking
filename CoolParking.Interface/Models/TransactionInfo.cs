﻿using Newtonsoft.Json;
using System;

namespace CoolParking.Interface.Models
{
    public class TransactionInfo
    {
        [JsonProperty("vehicle")]
        public string Vehicle { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("time")]
        public DateTime Time { get; set; }

    }
}
