﻿using System;
using System.Collections.Generic;

namespace CoolParking.Interface.ConsoleIO
{
    public static class EnumerableMethods
    {
        public static void ToLineList<T>(this IEnumerable<T> collection, string prompt)
        {
            Console.WriteLine(string.Concat(" " + prompt + ":" + "\n", string.Join("\n", collection), "\n"));
        }
    }
}
