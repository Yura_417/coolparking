﻿using CoolParking.Interface.ConsoleIO;
using CoolParking.Interface.ConsoleUI;
using CoolParking.Interface.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace CoolParking.Interface
{
    public class MainManager : CommandManager
    {
        protected override void IniCommandsInfo()
        {
            commandsInfo = new CommandInfo[]
            {
                new CommandInfo("Завершити роботу", null),
                new CommandInfo("Поточний баланс Паркінгу",CurrentBalanceOfParking),
                new CommandInfo("Кількість місць на паркінгу",Capacity),
                new CommandInfo("Кількість вільних місць на паркуванні",NumberOfVacancies),
                new CommandInfo("Транзакції Паркінгу за поточний період",ParkingTransactions),
                new CommandInfo("Історія Транзакцій",TransactionHistory),
                new CommandInfo("Список Тр. засобів на Паркінгу", ListOfVehicles),
                new CommandInfo("Поставити Тр. засіб на Паркінг", PutTheVehicle),
                new CommandInfo("Забрати Тр. засіб з Паркінгу", PickUpTheVehicle),
                new CommandInfo("Поповнити баланс Тр. засобу", TopUpTheBalanceOfTheVehicle),
            };
        }

        private HttpClient client;

        public MainManager(HttpClient client)
        {
            this.client = client;
        }

        private async void CurrentBalanceOfParking()
        {
            var response = await client.GetStringAsync("http://localhost:5000/api/parking/balance");
            Console.WriteLine("" + response);
            RequestForContinuation();
        }

        private async void Capacity()
        {
            var response = await client.GetStringAsync("http://localhost:5000/api/parking/capacity");
            Console.WriteLine("Вмістимість паркінгу: " + response);
            RequestForContinuation();
        }

        private async void NumberOfVacancies()
        {
            var response = await client.GetStringAsync("http://localhost:5000/api/parking/freePlaces");
            Console.WriteLine("\nКількість вільних місць:" + response);
            RequestForContinuation();
        }

        private async void ParkingTransactions()
        {
            var response = await client.GetStringAsync("http://localhost:5000/api/Transactions/last");
            Console.WriteLine("\n" + response);
            RequestForContinuation();
        }

        private async void TransactionHistory()
        {
            var response = await client.GetStringAsync("http://localhost:5000/api/Transactions/all");
            Console.WriteLine( "\n"+response);
            RequestForContinuation();
        }

        private async void ListOfVehicles()
        {
            var response = await client.GetStringAsync("http://localhost:5000/api/Vehicles");
            Console.WriteLine("\n" + response);
            RequestForContinuation();
        }

        private async void PutTheVehicle()
        {
                Vehicle inst = new Vehicle();
                inst.Id = "";
                int type = Entering.EnterInt32("Оберіть тип транспорту(1.PassengerCar, 2.Truck, 3.Bus, 4.Motorcycle)", 1, 4);
                switch (type)
                {
                    case 1:
                        inst.VehicleType = "PassengerCar";
                        break;
                    case 2:
                        inst.VehicleType = "Truck";
                        break;
                    case 3:
                        inst.VehicleType = "Bus";
                        break;
                    case 4:
                        inst.VehicleType = "Motorcycle";
                        break;
                }
                inst.Balance = Entering.EnterInt32("Balanse");
            var vehiclesJson = JsonConvert.SerializeObject(inst);
            var requestContent = new StringContent(vehiclesJson, Encoding.UTF8, "application/json");
            var responce = await client.PostAsync("http://localhost:5000/api/vehicles", requestContent);
            Console.WriteLine("\n" + responce.EnsureSuccessStatusCode());
        }

        private async void PickUpTheVehicle()
        {
            Vehicle inst = new Vehicle();
            inst.Id = Entering.EnterString("id");
            
            var responce = await client.DeleteAsync($"http://localhost:5000/api/vehicles/{inst.Id}");
            Console.WriteLine("\n" + responce.EnsureSuccessStatusCode());
        }

        private async void TopUpTheBalanceOfTheVehicle()
        {
            VehiclesTopUp inst = new VehiclesTopUp();
            inst.Id = Entering.EnterString("id");
            inst.Sum = Entering.EnterInt32("sum");

            var topUP = JsonConvert.SerializeObject(inst);
            var requestContent = new StringContent(topUP, Encoding.UTF8, "application/json");
            var responce = await client.PutAsync("http://localhost:5000/api/Transactions/topUpVehicle", requestContent);
            Console.WriteLine("\n" + responce.EnsureSuccessStatusCode());
            RequestForContinuation();
        }

        protected override void PrepareScreen()
        {
            Console.Clear();
            Console.WriteLine("---Інформація про Паркінг---");
        }
    }
}
