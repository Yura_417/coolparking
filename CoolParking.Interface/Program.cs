﻿using CoolParking.Interface.ConsoleIO;
using System;
using System.Net.Http;

namespace CoolParking.Interface
{
    class Program
    {
        static MainManager mainManager;

        static void Main(string[] args)
        {
            Settings.SetConsoleParam();
            Console.Title = "CoolParking";
            var client = new HttpClient();

            mainManager = new MainManager(client);
            mainManager.Run();
        }
    }
}
